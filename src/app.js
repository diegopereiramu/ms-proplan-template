import api from '@amacl/roboc-api';
import { apiOptions } from '@config';
import home from '@routes/home';

const run = () => {
    api(apiOptions,(router, app) => {
        home(router);
        return router;
    });
};

export {
    run,
}