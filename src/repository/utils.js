import { query } from './mysql.connect';

const getDate = async (user, users) => {
    const result = await query(`SELECT now() as date;`);
    return result[0].date;
};


export {
    getDate,
}
